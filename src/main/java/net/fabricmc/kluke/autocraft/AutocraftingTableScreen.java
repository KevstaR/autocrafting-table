package net.fabricmc.kluke.autocraft;

import io.github.cottonmc.cotton.gui.client.CottonInventoryScreen;
import net.fabricmc.kluke.autocraft.gui.AutocraftingTableGuiDescription;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.Text;

public class AutocraftingTableScreen extends CottonInventoryScreen<AutocraftingTableGuiDescription> {
    public AutocraftingTableScreen(AutocraftingTableGuiDescription gui, PlayerEntity player, Text title) {
        super(gui, player, title);
    }
}