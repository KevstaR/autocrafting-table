package net.fabricmc.kluke.autocraft;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.fabricmc.kluke.autocraft.gui.AutocraftingTableGuiDescription;

import static net.fabricmc.kluke.autocraft.AutocraftingTableMod.AUTOCRAFTING_TABLE_SCREEN_HANDLER;

@Environment(EnvType.CLIENT)
public class AutocraftingTableModClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        // don't be fooled, specifying the generics' types is necessary here
        ScreenRegistry.<AutocraftingTableGuiDescription, AutocraftingTableScreen>register(AUTOCRAFTING_TABLE_SCREEN_HANDLER,
                (gui, playerInventory, titleText) -> new AutocraftingTableScreen(gui, playerInventory.player, titleText));
    }
}
