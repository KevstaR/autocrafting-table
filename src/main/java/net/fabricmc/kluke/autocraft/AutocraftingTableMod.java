package net.fabricmc.kluke.autocraft;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.fabricmc.kluke.autocraft.gui.AutocraftingTableGuiDescription;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroups;
import net.minecraft.registry.Registries;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;
import net.minecraft.registry.Registry;

import static net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents.END_DATA_PACK_RELOAD;

public class AutocraftingTableMod implements ModInitializer {
	public static final String MOD_ID = "autocraft";
	public static final Identifier IDENTIFIER = new Identifier(MOD_ID, "table_block");

	public static final Block AUTOCRAFTING_TABLE_BLOCK;
	public static final BlockItem AUTOCRAFTING_TABLE_BLOCK_ITEM;
	public static final BlockEntityType<AutocraftingTableBlockEntity> AUTOCRAFTING_TABLE_BLOCK_ENTITY;
	public static final ScreenHandlerType<AutocraftingTableGuiDescription> AUTOCRAFTING_TABLE_SCREEN_HANDLER;

	static {
		AUTOCRAFTING_TABLE_BLOCK = Registry.register(Registries.BLOCK, IDENTIFIER, new AutocraftingTableBlock(FabricBlockSettings.create().solid().strength(2.5f)));
		AUTOCRAFTING_TABLE_BLOCK_ITEM = Registry.register(Registries.ITEM, IDENTIFIER, new BlockItem(AUTOCRAFTING_TABLE_BLOCK, new FabricItemSettings()));
		ItemGroupEvents.modifyEntriesEvent(ItemGroups.REDSTONE).register(entries -> entries.add(AUTOCRAFTING_TABLE_BLOCK_ITEM));
		AUTOCRAFTING_TABLE_BLOCK_ENTITY = Registry.register(Registries.BLOCK_ENTITY_TYPE, IDENTIFIER, FabricBlockEntityTypeBuilder.create(AutocraftingTableBlockEntity::new, AUTOCRAFTING_TABLE_BLOCK).build(null));
		AUTOCRAFTING_TABLE_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(IDENTIFIER, (world, playerInventory) -> AutocraftingTableGuiDescription.create(world, playerInventory, ScreenHandlerContext.EMPTY));
	}

	@Override
	public void onInitialize() {
		END_DATA_PACK_RELOAD.register((u1, u2, u3) -> AutocraftingTableBlockEntity.validator.update());
	}
}
