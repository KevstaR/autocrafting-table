package net.fabricmc.kluke.autocraft.gui;

import io.github.cottonmc.cotton.gui.SyncedGuiDescription;
import io.github.cottonmc.cotton.gui.widget.WItemSlot;
import io.github.cottonmc.cotton.gui.widget.WLabel;
import io.github.cottonmc.cotton.gui.widget.WPlainPanel;
import io.github.cottonmc.cotton.gui.widget.data.HorizontalAlignment;
import io.github.cottonmc.cotton.gui.widget.data.Insets;
import net.fabricmc.kluke.autocraft.inventory.CraftingGrid;
import net.fabricmc.kluke.autocraft.inventory.Slots;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.ScreenHandlerContext;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import org.jetbrains.annotations.NotNull;

import static net.fabricmc.kluke.autocraft.AutocraftingTableMod.AUTOCRAFTING_TABLE_SCREEN_HANDLER;
import static net.fabricmc.kluke.autocraft.AutocraftingTableMod.MOD_ID;
import static net.fabricmc.kluke.autocraft.gui.AutocraftingTableGuiDescription.Measurements.*;

public class AutocraftingTableGuiDescription extends SyncedGuiDescription {
    public static final ArrayPropertyDelegate EMPTY_DELEGATE = new ArrayPropertyDelegate(0);

    protected AutocraftingTableGuiDescription(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        super(AUTOCRAFTING_TABLE_SCREEN_HANDLER, syncId, playerInventory, inventory, EMPTY_DELEGATE);
    }

    @Override
    public void onSlotClick(int slotNumber, int button, SlotActionType action, PlayerEntity player) {
        if (action == SlotActionType.QUICK_MOVE) {
            if (slotNumber < Slots.INPUT_START) {
                // prevent duping items when shift-clicking out of template
                if (slotNumber >= 0) this.slots.get(slotNumber).setStack(ItemStack.EMPTY);
                return;
            } else
                // prevent shift-clicking into auto crafter
                if (slotNumber >= Slots.INVENTORY_SIZE) return;
        }
        super.onSlotClick(slotNumber, button, action, player);
    }

    public static AutocraftingTableGuiDescription create(int syncId, PlayerInventory playerInventory, ScreenHandlerContext context) {
        return create(syncId, playerInventory, getBlockInventory(context, Slots.INVENTORY_SIZE));
    }

    public static AutocraftingTableGuiDescription create(int syncId, PlayerInventory playerInventory, Inventory inventory) {
        final AutocraftingTableGuiDescription instance = new AutocraftingTableGuiDescription(syncId, playerInventory, inventory);
        WPlainPanel root = new WPlainPanel();
        root.setInsets(Insets.ROOT_PANEL);
        instance.setRootPanel(root);

        populate(inventory, root);

        root.add(instance.createPlayerInventoryPanel(), 0, PLAYER_INVENTORY_Y);

        // this validation must be last!
        root.validate(instance);

        return instance;
    }

    protected static void populate(Inventory inventory, WPlainPanel root) {
        root.add(createGrid(inventory, 0), TEMPLATE_X, GRID_Y);
        addLabel(root, ".template", TEMPLATE_LABEL_X);
        addInputGrid(root, inventory);
        addOutputSlot(root, OUTPUT_X, inventory);
    }

    protected static void addInputGrid(WPlainPanel root, Inventory inventory) {
        root.add(createGrid(inventory, CraftingGrid.SIZE), Measurements.INPUT_X, GRID_Y);
        addLabel(root, ".input", INPUT_LABEL_X);
    }

    @NotNull
    private static WItemSlot createGrid(Inventory inventory, int startIndex) {
        return WItemSlot.of(inventory, startIndex, CraftingGrid.WIDTH, CraftingGrid.HEIGHT);
    }

    protected static void addOutputSlot(WPlainPanel root, int outputX, Inventory inventory) {
        var slot = WItemSlot.outputOf(inventory, Slots.OUTPUT_SLOT).setInsertingAllowed(false);
        root.add(slot, outputX, OUTPUT_Y);
    }

    protected static void addLabel(WPlainPanel root, String suffix, int inputLabelX) {
        var text = Text.translatable("label." + MOD_ID + suffix);
        var wLabel = new WLabel(text).setHorizontalAlignment(HorizontalAlignment.CENTER);
        root.add(wLabel, inputLabelX, GRID_LABEL_Y);
    }

    @Override
    public ScreenHandlerType<?> getType() {
        return AUTOCRAFTING_TABLE_SCREEN_HANDLER;
    }
    
    public interface Measurements {
        int SLOT_WIDTH = 18;
        int INPUT_X = 4 * SLOT_WIDTH;
        int OUTPUT_X = 8 * SLOT_WIDTH - 5;
        int OUTPUT_Y = 2 * SLOT_WIDTH;
        int GRID_LABEL_Y = 4 * SLOT_WIDTH;
        int TEMPLATE_LABEL_X = SLOT_WIDTH;
        int INPUT_LABEL_X = 5 * SLOT_WIDTH;
        int PLAYER_INVENTORY_Y = 5 * SLOT_WIDTH;
        int GRID_Y_OFFSET = -4;
        int GRID_Y = SLOT_WIDTH + GRID_Y_OFFSET;
        int TEMPLATE_X = 0;
    }
}