package net.fabricmc.kluke.autocraft.mixin;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(CraftingInventory.class)
public interface CraftingInventoryAccessor {
    @Accessor("stacks")
    DefaultedList<ItemStack> autocrafting_table$getStacks();

    @Accessor("stacks")
    @Mutable
    void autocrafting_table$setStacks(DefaultedList<ItemStack> stacks);
}
