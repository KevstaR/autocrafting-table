package net.fabricmc.kluke.autocraft.inventory;

public interface CraftingGrid {
    int HEIGHT = 3;
    int WIDTH = 3;
    int SIZE = WIDTH * HEIGHT;

    static boolean contains(int slot) {
        return slot >= 0 && slot < SIZE;
    }
}
