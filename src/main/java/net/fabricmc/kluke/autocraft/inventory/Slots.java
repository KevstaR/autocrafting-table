package net.fabricmc.kluke.autocraft.inventory;

public interface Slots {
    int TEMPLATE_START = 0;
    int INPUT_START = CraftingGrid.SIZE;
    int LAST_TEMPLATE_SLOT = INPUT_START - 1;

    int INPUT_PLUS_OUTPUT_SIZE = CraftingGrid.SIZE + 1;

    int OUTPUT_SLOT = CraftingGrid.SIZE * 2;
    int INVENTORY_SIZE = OUTPUT_SLOT + 1;

    static int toInputSlot(int slot) {
        return slot - CraftingGrid.SIZE;
    }
}