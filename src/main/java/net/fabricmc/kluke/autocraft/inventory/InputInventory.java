package net.fabricmc.kluke.autocraft.inventory;

import net.fabricmc.fabric.api.transfer.v1.item.ItemVariant;
import net.fabricmc.fabric.api.transfer.v1.storage.Storage;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;

import static net.fabricmc.kluke.autocraft.AutocraftingTableBlockEntity.templatePredicate;

@SuppressWarnings("UnstableApiUsage")
public class InputInventory extends CraftingView {
    protected final Inventory templateInventory;
    public final Storage<ItemVariant> storage;

    public InputInventory(DefaultedList<ItemStack> stacks, Inventory templateInventory, Runnable onChange) {
        super(stacks);
        this.templateInventory = templateInventory;
        this.storage = new ReactiveStorage(this, null, onChange);
    }

    @Override
    public boolean isValid(int slot, ItemStack stack) {
        return super.isValid(slot, stack) && matchesTemplate(slot, stack);
    }

    protected boolean matchesTemplate(int slot, ItemStack inputStack) {
        return templatePredicate(inputStack, this.templateInventory.getStack(slot));
    }
}
