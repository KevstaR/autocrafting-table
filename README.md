# autocrafting-table

Autocrafting table for Minecraft.

Mainly a simplification of https://gitlab.com/supersaiyansubtlety/automated_crafting

Supports a basic template style crafting table. Works in single player and multiplayer.

### Recipe
![Recipe](readme-resources/recipe.png)

### How to use
Build hoppers feeding into the top and/or sides and the table will autocraft items at hopper speed.

Place a hopper or hopper minecart below the table to collect items from the output slot.

### Example screenshots

![Example1](readme-resources/example1.png)

![Example2](readme-resources/example2.png)